<?php

final class Parser
{
    /**
     * @var string
     */
    private $serverName = 'localhost';

    /**
     * @var string
     */
    private $username = 'root';

    /**
     * @var string
     */
    private $password = 'root';

    /**
     * @var PDO
     */
    private $db;

    /**
     * @var array
     */
    private $products = [];

    private const COLUMN_LIST =
        [
            'id',
            'name',
            'weight',
            'usage',
            'vendor_code',
            'price_msk',
            'quantity_msk',
            'price_spb',
            'quantity_spb',
            'price_samara',
            'quantity_samara',
            'price_saratov',
            'quantity_saratov',
            'price_kazan',
            'quantity_kazan',
            'price_nvs',
            'quantity_nvs',
            'price_che',
            'quantity_che',
            'price_linii_chelyabinsk',
            'quantity_linii_chelyabinsk',
        ];

    private const CITES =
        [
            'Москва' => 'msk',
            'Санкт-Петербург' => 'spb',
            'Самара' => 'samara',
            'Саратов' => 'saratov',
            'Казань' => 'kazan',
            'Новосибирск' => 'nvs',
            'Челябинск' => 'che',
            'Деловые линии Челябинск' => 'linii_chelyabinsk',
        ];

    public function __construct()
    {
        $this->connectionDB();
    }

    /**
     * @param $path
     * @return array
     */
    public function run(string $path)
    {
        $this->receiveProducts($path);
        $this->saveDB();

        return $this->products;
    }

    /**
     * @param string $path
     * @return array
     */
    public function receiveProducts(string $path): array
    {
        echo "Собранный данные из XML\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

        for ($i = 0; $i < 8; $i++) {
            $pathImport = sprintf('%s/import%s_1.xml', $path, $i);
            $pathOffers = sprintf('%s/offers%s_1.xml', $path, $i);
            $startTime = microtime(true);

            $importData = $this->receiveFile($pathImport);
            $cityKey = $this->receiveCity($importData);

            echo $cityKey;

            $this->receiveImportData($importData);

            $offersData = $this->receiveFile($pathOffers);
            $city = self::CITES[$cityKey];

            $this->receiveOffersData($offersData, $city);

            $endTime = microtime(true) - $startTime;

            echo sprintf(" -> %s \n", $endTime);
        }

        return $this->products;
    }

    /**
     * @param string $path
     * @return array
     */
    public function receiveFile(string $path): array
    {
        return
            json_decode(
                json_encode(
                    simplexml_load_string(file_get_contents($path))
                ),
                true
            );
    }

    /**
     * @param $table
     * @param array $rows
     * @param array $columns
     * @return bool
     */
    public function batchInsert($table, array $rows, array $columns = array())
    {
        // Is array empty? Nothing to insert!
        if (empty($rows)) {
            return true;
        }

        // Get the column count. Are we inserting all columns or just the specific columns?
        $columnCount = !empty($columns) ? count($columns) : count(reset($rows));

        // Build the column list
        $columnList = !empty($columns) ? '(`' . implode('`, `', $columns) . '`)' : '';


        // Build value placeholders for single row
        $rowPlaceholder = ' (' . implode(', ', array_fill(1, $columnCount, '?')) . ')';


        // Build the whole prepared query
        $query = sprintf(
            'INSERT INTO %s%s VALUES %s',
            $table,
            $columnList,
            implode(', ', array_fill(1, count($rows), $rowPlaceholder))
        );


        // Prepare PDO statement
        $statement = $this->db->prepare($query);

        // Flatten the value array (we are using ? placeholders)
        $data = [];

        foreach ($rows as $rowData) {
            $data = array_merge($data, array_values($rowData));
        }

        // Did the insert go successfully?
        return $statement->execute($data);
    }

    /**
     * @return $this
     */
    private function connectionDB()
    {
        try {
            $dsn = sprintf('mysql:host=%s;dbname=task_sionic;charset=utf8', $this->serverName);
            $db = new PDO($dsn, $this->username, $this->password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db = $db;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

        return $this;
    }

    /**
     * @param string $source
     * @return string
     */
    private function receiveCity(array $source): string
    {
        return
            trim(
                preg_replace(
                    '~.*\(|\).*~sui',
                    '',
                    $source['Классификатор']['Наименование'] ?? ''
                )
            );
    }

    /**
     * @param array $source
     * @return mixed[]
     */
    private function receiveImportData(array $source)
    {
        foreach ($source['Каталог']['Товары']['Товар'] as $index => $product) {
            if (empty($product['Наименование'])) {
                continue;
            }

            $productData = $this->products[$product['Код']] ?? [];

            $this->products[$product['Код']] =
                [
                    'id' => $product['Код'],
                    'name' => $product['Наименование'],
                    'weight' => $product['Вес'] ?? '',
                    'usage' => $this->receiveUsage($product),
                    'vendor_code' => $productData['vendor_code'] ?? '',
                    'price_msk' => $productData['price_msk'] ?? '',
                    'quantity_msk' => $productData['quantity_msk'] ?? '',
                    'price_spb' => $productData['price_spb'] ?? ' ',
                    'quantity_spb' => $productData['quantity_spb'] ?? '',
                    'price_samara' => $productData['price_samara'] ?? '',
                    'quantity_samara' => $productData['quantity_samara'] ?? '',
                    'price_saratov' => $productData['price_saratov'] ?? ' ',
                    'quantity_saratov' => $productData['quantity_saratov'] ?? '',
                    'price_kazan' => $productData['price_kazan'] ?? ' ',
                    'quantity_kazan' => $productData['quantity_kazan'] ?? '',
                    'price_nvs' => $productData['price_nvs'] ?? '',
                    'quantity_nvs' => $productData['quantity_nvs'] ?? '',
                    'price_che' => $productData['price_che'] ?? '',
                    'quantity_che' => $productData['quantity_che'] ?? '',
                    'price_linii_chelyabinsk' => $productData['price_linii_chelyabinsk'] ?? '',
                    'quantity_linii_chelyabinsk' => $productData['quantity_linii_chelyabinsk'] ?? '',
                ];
        }

        return $this->products;
    }

    /**
     * @param array $product
     * @return string
     */
    private function receiveUsage(array $product): string
    {
        $usage = '';

        if (empty($product['Взаимозаменяемости']['Взаимозаменяемость'])) {
            return $usage;
        }

        foreach ($product['Взаимозаменяемости']['Взаимозаменяемость'] as $index => $interchangeability) {
            $checkUsage = (
                empty($interchangeability['Марка'])
                or empty($interchangeability['Модель'])
                or empty($interchangeability['КатегорияТС'])
            );

            if ($checkUsage) {
                continue;
            }

            $usage = sprintf('%s|%s', $usage, implode('-', $interchangeability));
        }

        return trim($usage, '|');
    }

    /**
     * @param array $source
     * @param string $city
     * @return mixed[]
     */
    private function receiveOffersData(array $source, string $city)
    {
        foreach ($source['ПакетПредложений']['Предложения']['Предложение'] as $offer) {
            if (empty($this->products[$offer['Код']])) {
                continue;
            }

            if (is_array($offer['Артикул'] ?? '')) {
                $offer['Артикул'] = '';
            }

            $this->products[$offer['Код']]['price_' . $city] = $offer['Цены']['Цена'][0]['ЦенаЗаЕдиницу'] ?? 0;
            $this->products[$offer['Код']]['vendor_code'] = $offer['Артикул'] ?? '';
            $this->products[$offer['Код']]['quantity_' . $city] = $offer['Количество'] ?? '';
        }

        return $this->products;
    }

    /**
     * @void
     */
    private function saveDB()
    {
        echo "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n Запис в База Даних \n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

        $startTime = microtime(true);
        $this->truncateProducts();
        $this->batchInsert('products', $this->products, self::COLUMN_LIST);
        $endTime = microtime(true) - $startTime;

        echo sprintf("%s \n", $endTime);
    }

    /**
     * @return false|PDOStatement
     */
    private function truncateProducts()
    {
        return $this->db->prepare('TRUNCATE TABLE `products`')->execute();
    }
}

$startTime = microtime(true);

$parser = new Parser();
$products = $parser->run(sprintf('%s/data', __DIR__));

$endTime = microtime(true) - $startTime;

echo sprintf("\n\n Total - %s \n\n", $endTime);
